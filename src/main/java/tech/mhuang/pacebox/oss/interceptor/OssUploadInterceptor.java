package tech.mhuang.pacebox.oss.interceptor;

import tech.mhuang.pacebox.core.chain.BaseChain;
import tech.mhuang.pacebox.core.chain.BaseInterceptor;
import tech.mhuang.pacebox.core.exception.BusinessException;
import tech.mhuang.pacebox.core.util.ObjectUtil;
import tech.mhuang.pacebox.oss.BaseOssHandler;
import tech.mhuang.pacebox.oss.domain.OssUploadRequest;
import tech.mhuang.pacebox.oss.domain.OssUploadResult;

import java.util.Map;

/**
 * oss上传接口拦截
 *
 * @author mhuang
 * @since 1.0.0
 */
public interface OssUploadInterceptor extends BaseInterceptor<BaseChain<OssUploadRequest, OssUploadResult>, OssUploadResult> {

    class Standard implements OssUploadInterceptor {

        private final Map<String, BaseOssHandler> ossHandlerMap;

        public Standard(Map<String, BaseOssHandler> ossHandlerMap) {
            this.ossHandlerMap = ossHandlerMap;
        }

        @Override
        public OssUploadResult interceptor(BaseChain<OssUploadRequest, OssUploadResult> chain) {
            OssUploadRequest request = chain.request();
            OssUploadResult result;
            BaseOssHandler ossHandler = ossHandlerMap.get(request.getType());
            if (ObjectUtil.isEmpty(ossHandler)) {
                result = OssUploadResult.builder().success(false)
                        .message("找不到上传的OSS配置")
                        .throwable(new BusinessException(500, "找不到上传的OSS配置")).build();
            } else {
                result = ossHandler.upload(request);
            }
            return result;
        }
    }
}