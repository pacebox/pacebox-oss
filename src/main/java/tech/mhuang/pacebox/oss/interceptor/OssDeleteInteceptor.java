package tech.mhuang.pacebox.oss.interceptor;

import tech.mhuang.pacebox.core.chain.BaseChain;
import tech.mhuang.pacebox.core.chain.BaseInterceptor;
import tech.mhuang.pacebox.core.exception.BusinessException;
import tech.mhuang.pacebox.core.util.ObjectUtil;
import tech.mhuang.pacebox.oss.BaseOssHandler;
import tech.mhuang.pacebox.oss.domain.OssDeleteRequest;
import tech.mhuang.pacebox.oss.domain.OssDeleteResult;

import java.util.Map;

/**
 * oss删除接口拦截
 *
 * @author mhuang
 * @since 1.0.0
 */
public interface OssDeleteInteceptor extends BaseInterceptor<BaseChain<OssDeleteRequest, OssDeleteResult>, OssDeleteResult> {

    class Standard implements OssDeleteInteceptor {

        private final Map<String, BaseOssHandler> ossHandlerMap;

        public Standard(Map<String, BaseOssHandler> ossHandlerMap) {
            this.ossHandlerMap = ossHandlerMap;
        }


        @Override
        public OssDeleteResult interceptor(BaseChain<OssDeleteRequest, OssDeleteResult> chain) {
            OssDeleteRequest request = chain.request();
            OssDeleteResult result;
            BaseOssHandler ossHandler = ossHandlerMap.get(request.getType());
            if (ObjectUtil.isEmpty(ossHandler)) {
                result = OssDeleteResult.builder().success(false)
                        .message("找不到删除的OSS配置")
                        .throwable(new BusinessException(500, "找不到删除的OSS配置")).build();
            } else {
                result = ossHandler.delete(request);
            }
            return result;
        }
    }
}