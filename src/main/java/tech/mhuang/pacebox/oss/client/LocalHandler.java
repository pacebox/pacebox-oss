package tech.mhuang.pacebox.oss.client;

import tech.mhuang.pacebox.core.exception.ExceptionUtil;
import tech.mhuang.pacebox.core.file.FileUtil;
import tech.mhuang.pacebox.oss.BaseOssHandler;
import tech.mhuang.pacebox.oss.OssFieldProperties;
import tech.mhuang.pacebox.oss.domain.OssDeleteRequest;
import tech.mhuang.pacebox.oss.domain.OssDeleteResult;
import tech.mhuang.pacebox.oss.domain.OssDownloadRequest;
import tech.mhuang.pacebox.oss.domain.OssDownloadResult;
import tech.mhuang.pacebox.oss.domain.OssStorageType;
import tech.mhuang.pacebox.oss.domain.OssUploadRequest;
import tech.mhuang.pacebox.oss.domain.OssUploadResult;

import java.io.File;

/**
 * 本地文件下载
 *
 * @author mhuang
 * @since 1.0.0
 */
public class LocalHandler implements BaseOssHandler {

    private OssFieldProperties properties;

    @Override
    public void setProperties(OssFieldProperties properties) {
        this.properties = properties;
    }

    @Override
    public OssUploadResult upload(OssUploadRequest request) {
        OssUploadResult.OssUploadResultBuilder result = OssUploadResult.builder().extendParam(request.getExtendParam());
        try {
            File targetFile = new File(new StringBuilder().append(request.getBucketName())
                    .append(File.separator)
                    .append(request.getKey())
                    .toString()
            );
            if (request.getStorageType() == OssStorageType.LOCAL) {
                File sourceFile = new File(request.getLocalUrl());
                FileUtil.copyFile(sourceFile, targetFile);
                if (request.isDelFile()) {
                    sourceFile.delete();
                }
            } else if (request.getStorageType() == OssStorageType.BYTE) {
                FileUtil.writeByteArrayToFile(targetFile, request.getByteArray());
            } else if (request.getStorageType() == OssStorageType.STREAM) {
                FileUtil.copyToFile(request.getInStream(), targetFile);
            } else if (request.getStorageType() == OssStorageType.FILE) {
                FileUtil.copyFile(request.getFile(), targetFile);
                if (request.isDelFile()) {
                    request.getFile().delete();
                }
            }
            result.success(true);
        } catch (Exception e) {
            result.message(ExceptionUtil.getMessage(e)).throwable(e);
        }
        return result.build();
    }

    @Override
    public OssDownloadResult download(OssDownloadRequest request) {
        OssDownloadResult.OssDownloadResultBuilder builder = OssDownloadResult.builder().extendParam(request.getExtendParam());
        try {
            builder.byteArray(FileUtil.readFileToByteArray(new File(request.getBucketName() + request.getKey())))
                    .success(true);
        } catch (Exception e) {
            builder.message(ExceptionUtil.getMessage(e)).throwable(e);
        }
        return builder.build();
    }

    @Override
    public OssDeleteResult delete(OssDeleteRequest request) {
        OssDeleteResult.OssDeleteResultBuilder builder = OssDeleteResult.builder();
        builder.extendParam(request.getExtendParam());
        try {
            builder.success(new File(request.getBucketName() + request.getKey()).delete());
        } catch (Exception e) {
            builder.throwable(e).message(ExceptionUtil.getMessage(e));
        }
        return builder.build();
    }
}
