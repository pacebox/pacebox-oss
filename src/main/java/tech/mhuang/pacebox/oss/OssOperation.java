package tech.mhuang.pacebox.oss;

import tech.mhuang.pacebox.oss.domain.OssDeleteRequest;
import tech.mhuang.pacebox.oss.domain.OssDeleteResult;
import tech.mhuang.pacebox.oss.domain.OssDownloadRequest;
import tech.mhuang.pacebox.oss.domain.OssDownloadResult;
import tech.mhuang.pacebox.oss.domain.OssUploadRequest;
import tech.mhuang.pacebox.oss.domain.OssUploadResult;
import tech.mhuang.pacebox.oss.interceptor.OssDeleteInteceptor;
import tech.mhuang.pacebox.oss.interceptor.OssDownloadInterceptor;
import tech.mhuang.pacebox.oss.interceptor.OssUploadInterceptor;

/**
 * oss操作
 *
 * @author mhuang
 * @since 1.0.0
 */
public interface OssOperation {
    /**
     * 添加OSS处理器
     *
     * @param key        OSS处理的key
     * @param ossHandler OSS处理类
     */
    void addHandler(String key, BaseOssHandler ossHandler);

    /**
     * 添加OSS上传拦截器
     *
     * @param uploadInterceptor OSS上传拦截器
     */
    void addUploadIntecptor(OssUploadInterceptor uploadInterceptor);

    /**
     * 添加OSS下载拦截器
     *
     * @param downloadInterceptor oss下载拦截器
     */
    void addDownloadInteceptor(OssDownloadInterceptor downloadInterceptor);

    /**
     * 添加OSS删除拦截器
     *
     * @param deleteInteceptor oss删除拦截器
     */
    void addDeleteInteceptor(OssDeleteInteceptor deleteInteceptor);

    /**
     * oss上传
     *
     * @param uploadRequest oss上传对象
     * @return oss上传结果
     */
    OssUploadResult upload(OssUploadRequest uploadRequest);

    /**
     * oss下载
     *
     * @param downloadRequest oss下载的请求对象
     * @return oss下载结果
     */
    OssDownloadResult download(OssDownloadRequest downloadRequest);

    /**
     * oss删除
     *
     * @param deleteRequest oss删除的请求对象
     * @return oss删除结果
     */
    OssDeleteResult delete(OssDeleteRequest deleteRequest);
}