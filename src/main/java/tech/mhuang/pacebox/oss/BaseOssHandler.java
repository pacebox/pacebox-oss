package tech.mhuang.pacebox.oss;

import tech.mhuang.pacebox.oss.domain.OssDeleteRequest;
import tech.mhuang.pacebox.oss.domain.OssDeleteResult;
import tech.mhuang.pacebox.oss.domain.OssDownloadRequest;
import tech.mhuang.pacebox.oss.domain.OssDownloadResult;
import tech.mhuang.pacebox.oss.domain.OssUploadRequest;
import tech.mhuang.pacebox.oss.domain.OssUploadResult;

import java.io.IOException;

/**
 * oss处理接口
 *
 * @author mhuang
 * @since 1.0.0
 */
public interface BaseOssHandler {

    /**
     * 设置属性
     *
     * @param properties 基础属性
     */
    void setProperties(OssFieldProperties properties) ;

    /**
     * 文件上传
     *
     * @param request 文件相关属性
     * @return 文件上传结果
     */
    OssUploadResult upload(OssUploadRequest request);

    /**
     * 文件下载
     *
     * @param request 文件下载请求
     * @return 文件下载结果
     */
    OssDownloadResult download(OssDownloadRequest request);

    /**
     * 文件删除
     *
     * @param request 文件删除请求
     * @return 文件删除结果
     */
    OssDeleteResult delete(OssDeleteRequest request);
}
