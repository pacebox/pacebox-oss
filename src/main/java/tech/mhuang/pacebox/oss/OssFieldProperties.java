package tech.mhuang.pacebox.oss;

import lombok.Data;

/**
 * OSS配置类
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
public class OssFieldProperties {

    /**
     * 采用得OSS实现.
     * 自行实现需实现<code>tech.mhuang.pacebox.oss.BaseOssHandler</code>接口
     */
    private Class<? extends BaseOssHandler> driver;

    /**
     * 区域
     */
    private String region;

    /**
     * accessKey
     */
    private String accessKey;

    /**
     * accessSecret
     */
    private String accessSecret;

    /**
     * 是否使用代理
     */
    private boolean useProxy;

    /**
     * 代理主机
     */
    private String proxyHost;

    /**
     * 代理端口
     */
    private int proxyPort;

    /**
     * 代理类型（1代表FTP）
     */
    private String proxyType;

    /**
     * 代理的用户名
     */
    private String proxyUsername;

    /**
     * 代理的密码
     */
    private String proxyPwd;
}
