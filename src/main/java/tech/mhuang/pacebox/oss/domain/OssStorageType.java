package tech.mhuang.pacebox.oss.domain;


/**
 * 存储方式
 *
 * @author mhuang
 * @since 1.0.0
 */
public enum OssStorageType {
    LOCAL("1"),
    BYTE("2"),
    STREAM("3"),
    FILE("4");
    private String type;

    OssStorageType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }
}
