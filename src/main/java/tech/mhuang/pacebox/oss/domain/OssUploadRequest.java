package tech.mhuang.pacebox.oss.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import tech.mhuang.pacebox.core.dict.BasicDict;

import java.io.File;
import java.io.InputStream;

/**
 * @author mhuang
 * @since 1.0.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OssUploadRequest {

    /**
     * 对应的Type
     */
    private String type;

    /**
     * 存放得文件夹
     */
    private String folder;

    /**
     * 存储空间名称
     */
    private String bucketName;

    /**
     * 上传的key(若是本地则文件名称+后缀）
     */
    private String key;
    /**
     * 上传到oss文件名称
     */
    private String fileName;
    /**
     * 发送存储类型
     */
    private OssStorageType storageType;

    /**
     * 当存储类型是Local时写入
     */
    private String localUrl;

    /**
     * 当存储类型是byte时写入
     */
    private byte[] byteArray;

    /**
     * 当存储类型是stream时写入
     */
    private InputStream inStream;

    /**
     * 当存储类型是File时写入
     */
    private File file;

    /**
     * 当存储类型是File 或者是本地文件时使用，是否删除原文件
     */
    private boolean delFile;

    /**
     * 扩展参数、用于自行业务处理(采用回调时会带入)
     */
    private BasicDict extendParam;
}
