package tech.mhuang.pacebox.oss.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import tech.mhuang.pacebox.core.dict.BasicDict;

/**
 * oss发送
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class OssUploadResult {

    /**
     * 请求id
     */
    private String requestId;

    /**
     * etag值
     */
    private String etag;

    /**
     * 真实状态
     */
    private boolean success;

    /**
     * 短信反馈本文
     */
    private String message;

    /**
     * 扩展参数、用于自行业务处理
     */
    private BasicDict extendParam;

    /**
     * 异常
     */
    private Throwable throwable;

}
