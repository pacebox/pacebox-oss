package tech.mhuang.pacebox.oss.domain;

import lombok.Data;
import tech.mhuang.pacebox.core.dict.BasicDict;

/**
 * 文件删除
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
public class OssDeleteRequest {

    private String type;

    /**
     * 存储名称
     */
    private String bucketName;

    /**
     * 删除的key
     */
    private String key;

    /**
     * 扩展参数、用于自行业务处理
     */
    private BasicDict extendParam;
}
