package tech.mhuang.pacebox.oss.domain;

import lombok.Data;
import tech.mhuang.pacebox.core.dict.BasicDict;

/**
 * 文件下载请求类
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
public class OssDownloadRequest {

    /**
     * 扩展字段
     */
    private String type;
    /**
     * 存储空间名称
     */
    private String bucketName;

    /**
     * 下载的key
     */
    private String key;

    /**
     * 扩展参数、用于自行业务处理(采用回调时会带入)
     */
    private BasicDict extendParam;

}
